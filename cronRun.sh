#!/bin/bash

myName=$0
myDir=`dirname ${myName}`
cd ${myDir}

rm -f output/*

publish_link=https://cms-tracker-hybrids-production.web.cern.ch/
targetDir=/eos/project/c/cmsweb/www/tracker-upgrade/hybridProduction
outputDir="${targetDir}/output"
mkdir "${targetDir}"
mkdir "${outputDir}"
cp html/index.php "$targetDir"
./cumulativePlot.py

if [ -d ${outputDir} ] ; then
	cp output/* "${outputDir}"
fi

