<!DOCTYPE html>

<html lang="en-US">
<head>
<title>CMS Tracker hybrid production</title>
<meta charset="UTF-8"> 
</head>
	<body>
		<h3>Hybrid production plots</h3>
		<a href='output/NInspectedHybrid_Time_2SFEHproduction.pdf'><img alt="A plot"  src='output/NInspectedHybrid_Time_2SFEHproduction.png' ></a>
		<a href='output/NInspectedHybrid_Time_2SSEHproduction.pdf'><img alt="A plot"  src='output/NInspectedHybrid_Time_2SSEHproduction.png' ></a>
		<a href='output/NInspectedHybrid_Time_PSFEHproduction.pdf'><img alt="A plot"  src='output/NInspectedHybrid_Time_PSFEHproduction.png' ></a>
		<a href='output/NInspectedHybrid_Time_PSPOHproduction.pdf'><img alt="A plot"  src='output/NInspectedHybrid_Time_PSPOHproduction.png' ></a>
		<a href='output/NInspectedHybrid_Time_PSROHproduction.pdf'><img alt="A plot"  src='output/NInspectedHybrid_Time_PSROHproduction.png' ></a>
<?php

// Specify the file path relative to the current script
$infoFilePath = './output/info.html';

// Check if the file exists
if (file_exists($infoFilePath)) {
    // Include the file if it exists
    include($infoFilePath);
}

?>
<pre>
Check last run at <?php include 'output/lastrun.txt';?>
</pre>
</body>
</html>
